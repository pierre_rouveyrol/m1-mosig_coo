public class Palindrome
{
    public static void main(String [] arg)
    {
        String input = arg[0];

        for(int i=1; i<input.length(); i++)
        {
            if( !(input.charAt(i)==input.charAt(input.length()-(i+1))) )
            {
                System.out.print("This is NOT a palindrome.");
                System.exit(0);
            }
        }
         System.out.print("This is a palindrome.");
    }
}
