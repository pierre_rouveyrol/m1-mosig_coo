import java.io .*;
/* Read an integer */
public class Saisie 
{
    public static void main (String [] arg) throws IOException
    {
        int val = 0;
        int sum = 0;
        BufferedReader inr = new BufferedReader(new InputStreamReader(System.in));
        while(val != -1)
        {
            System.out.println("Write an integer");
            val = Integer.parseInt(inr.readLine());
            if(val >= 0)
            {
                sum += val;
                System.out.println("The sum is " + sum + " enter another one or -1 to exit.");
            }
            else if(val < 0 && val != -1)
                System.out.println("You must enter positive integers, save -1 to exit");
         }
    }
}
