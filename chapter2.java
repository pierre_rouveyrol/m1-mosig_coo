/*Author : Pierre Rouveyrol <rouveyrolpierre@gmail.com>
 *
 * Answers to the questions here :
 * the toSting method is useful because if it wasn't present printing a pair 
 * would return the address of that particular pair, which is not very practical.
 * Instead, when willing to cast the object into a string,
 * the program automatically uses the provided toString method.*/


class Pair {
    // Pair (a , b )
    private int a , b ;
 
    
    
    //getters
    public int geta()
    {
        return a;
    }

    public int getb()
    {
        return b;
    }
   
    
    
    // constructors
    Pair (int a , int b)
    {
        this.a = a;
        this.b = b;
    }

    // clone
    Pair (Pair c)
    {
        this(c.geta(), c.getb());
    }

    
    
    // operators
    // strictly less
    public boolean lt (Pair c)
    {
        if(a < c.geta())
            return true;
        else if (a == c.geta() && b<c.getb())
            return true;
        else
            return false;
    }

    // equal
    public boolean eq (Pair c)
    {
        return(!lt(c) && !c.lt(this));
    }

    // not equal
    public boolean neq (Pair c)
    {
        return(!eq(c));
    }

    // less or equal
    public boolean leq (Pair c)
    {
        return(lt(c)||eq(c)); 
    }

    // strictly greater
    public boolean gt (Pair c)
    {
        return(!leq(c)); 
    }

    // greater or equal
    public boolean geq (Pair c)
    {
        return(gt(c)||eq(c)); 
    }

    public String toString ()
    {
        return("(" + this.a + ";" + this.b + ")");
    }
}

/*Sorry I don't understand what's asked for should I use an ArrayList*/
class PairsArray
{
    // default bloc size
    private static final int DEFAULT =10;
    // static array
    private Pair [] tab;
    // size of one bloc
    private final int n;
    // number of elements
    private int nbElt;
    


    // constructors
    PairsArray (final int n)
    {
        n = DEFAULT;
    }
    
    PairsArray ()
    {
    /* to complete */ 
    }
    


    public int size ()
    {
    /* to complete */ 
    }
    
    // test if there is no element
    public boolean empty ()
    {
    /* to complete */ 
    }
    
    public Pair value (int i)
    {
    /* to complete */ 
    }
    
    // add an element at the end of the array
    public void add (Pair c)
    {
    /* to complete */ 
    }
    
    // remove the last element
    public Pair remove ()
    {
    /* to complete */ 
    }
    
    public void modify (int i, Pair c)
    {
    /* to complete */ 
    }
    
    public void insert (int i, Pair c)
    {
    /* to complete */ 
    }
    
    public Pair remove (int i)
    {
    /* to complete */ 
    }
    
    public String toString ()
    {
    /* to complete */ 
    }
}

