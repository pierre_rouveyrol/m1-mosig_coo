/*This programs calculates all the primes inferior to 100*/
public class Primes
{
    public static void main (String [] arg)
    {
        boolean [] tab= new boolean[101];
        int j;
        java.util.Arrays.fill(tab, true);
        tab[0] = false; tab[1]= false;
        for(int i=0; i<100; i++)
        {
            if(tab[i])
            {
                j=2*i;
                while(j<=100)
                {
                    tab[j]=false;
                    j+=i;
                }
            }
        }
        for(int i=0; i<100; i++)
        {
            if(tab[i])
            System.out.print(i + "; ");
        }
    }
}

