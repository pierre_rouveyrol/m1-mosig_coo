/*This programs calculates all the primes inferior to 100*/
public class PrimesExt
{
    public static void main (String [] arg)
    {
        int number = Integer.parseInt(arg[0]);
        boolean [] tab= new boolean[number + 1];
        int j;
        java.util.Arrays.fill(tab, true);
        tab[0] = false; tab[1]= false;
        for(int i=0; i<number; i++)
        {
            if(tab[i])
            {
                j=2*i;
                while(j<=number)
                {
                    tab[j]=false;
                    j+=i;
                }
            }
        }
        for(int i=0; i<number; i++)
        {
            if(tab[i])
            System.out.print(i + "; ");
        }
    }
}

