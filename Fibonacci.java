public class Fibonacci
{
    public static void main (String [] arg)
    {
        int tab[] = new int[30];
        tab[0]=0;
        tab[1]=1;
        for(int i=2; i<=29; i++)
            tab[i]=tab[i-1]+tab[i-2];
        for(int i=29; i>=0; i--)
            System.out.print(tab[i] + "; ");
    }
}
