import java.util.*;

public class PascalTriangle
{
    public static void printList(ArrayList l)
    {
        for (int i=0; i<l.size(); i++) 
        {   
            System.out.print(l.get(i) + " ");
        }
        System.out.print("\n");
    }

    public static void copyList(List<Integer> source, List<Integer> dest)
    {
        dest.clear();
        for (Integer item : source) 
        {
            dest.add(item); 
        }
    }

    public static void main(String [] arg)
    {
        ArrayList<Integer> l = new ArrayList<Integer>();
        ArrayList<Integer> temp = new ArrayList<Integer>();
        int nIter = Integer.parseInt(arg[0])-2;
        
        l.add(1);
        printList(l);
        l.add(1);
        printList(l);

        for(int i=0; i<nIter; i++)
        {
            l.add(1);
            copyList(l,temp);
            for(int j=1; j<l.size()-1; j++)
            {    
                l.set(j,temp.get(j-1)+temp.get(j));
            }
            printList(l);
        }
    }
}
