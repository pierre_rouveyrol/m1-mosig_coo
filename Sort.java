/* Bubble Sort */
public class Sort
{
    public static void bubble (int [] tab)
    {
        boolean swap;
        int swapbuffer;
        do{
            swap = false;
            for(int i=0; i<tab.length-1; i++)
            {
                if(tab[i]>tab[i+1])
                {
                    swapbuffer=tab[i];
                    tab[i]=tab[i+1];
                    tab[i+1]=swapbuffer;
                    swap = true;
                }
            }
        }while(swap);
    }
    public static void main (String [] arg)
    {
        int t [] = {2 ,3 ,7 ,5 ,6 ,11 ,0};
        System.out.println(" Before ");
        for(int i=0; i<t.length; i++)
            System.out.print(t[i]+" ");
        System.out.println("\nAfter");
        Sort.bubble(t);
        for(int i=0; i<t.length; i++)
            System.out.print(t[i]+" ");
    }

}
