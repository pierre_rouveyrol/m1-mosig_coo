import java.lang.Math;

public class Syracuse
{
    public static void main(String [] arg)
    {
        int current = Math.random().intValue;
        System.out.print("seed = " + current + "; ");
        while(current !=0 && current !=1)
        {
            if(current%2==0)
                current = current/2;
            else
                current = (current*3)+1;
            System.out.print(current + "; ");
        }
    }
}
